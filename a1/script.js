console.log('s24 Activity');
console.log('');

// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
let getCube = 0;
const anyNum = 2;
const cubeNum = 3;
getCube = anyNum ** cubeNum;
// Template Literals
// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
console.log(`Template Literals:\nThe cube of ${anyNum} is: ${getCube}`);
console.log('');
// 5. Create a variable address with a value of an array containing details of an address.
const address = ["123", "Taguig City", "Metro Manila"];

// 6. Destructure the array and print out a message with the full address using Template Literals.
const [a, b, c] = address;
console.log(`Destructure the array:\nI live at ${a} ${b}, ${c}.`);
console.log('');
// Object Destructuring
// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
const animal = {
    name: 'Uggie',
    type: 'Dog',
    breed: 'Jack Russell terrier',
    weight:	'7.3 kg',
    knownFor: '"The Artist and Water for Elephants"',
}

// 8. Destructure the object and print out a message with the details of the animal using Template Literals.
const {name: aName, type: aType, breed: aBreed, weight: aWeight, knownFor: aKnownFor} = animal;
console.log(`Destructure the object:\n${aName} was a ${aType} with a breed of ${aBreed}. He weighed at ${aWeight} and he's known for ${aKnownFor}.`);
console.log('');
// Arrow Functions
// 9. Create an array of numbers.
const arrNumbers = [1, 2, 3, 4, 5];

// A
// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
console.log('The array using forEach():');
arrNumbers.forEach((num) => console.log(num));
console.log('');
// B
// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.
const reduceNumber = arrNumbers.reduce((p, c) => p + c ,0);
console.log('The array using reduce():');
console.log(reduceNumber);
console.log('');
// Javascript Objects
// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
class Dog {
    constructor(name, age, breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

// 13. Create/instantiate a new object from the class Dog and console log the object.
const myDog = new Dog('Mitsui', 7, 'Shih-Tzu');
console.log("New object from the class Dog:\n", myDog);